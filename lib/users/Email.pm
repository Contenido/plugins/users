package users::Email;

use base "Contenido::Document";
use Contenido::Globals;

use overload (
	'""'	=> '_stringify',
);

sub extra_properties
{
	return (
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'E-mail' },
		{ 'attr' => 'status',		'type' => 'status',     'rusname' => 'Статус e-mail-адреса',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Подтвержден'],
					[3, 'Потерян / заблокирован'],
				],
		},
		{ 'attr' => 'name_orig',	'type' => 'string',	'rusname' => 'E-mail без приведения к нижнему регистру' },
#		{ 'attr' => 'login',		'type' => 'string',	'rusname' => 'Login' },
#		{ 'attr' => 'passwd',		'type' => 'string',	'rusname' => 'Password' },
	)
}

sub _stringify {
    my $self = shift;

    return $self->name;
}


sub class_name
{
	return 'E-mail адрес';
}

sub class_description
{
	return 'E-mail адрес';
}

sub search_fields
{
	return ('name');
}

sub class_table
{
	return 'users::SQL::CredentialsTable';
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->main == 1 || $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:black;';
	}
}

sub pre_store
{
	my $self = shift;

	foreach my $attr ( qw(name name_orig) ) {
		$self->{$attr} =~ s/^\s+//s;
		$self->{$attr} =~ s/\s+$//s;
	}

	my $default_section = $project->s_alias->{user_emails}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{user_emails};
	if ( $default_section && !$self->{sections} ) {
		$self->{sections} = $default_section;
	}

	return 1;
}
1;
