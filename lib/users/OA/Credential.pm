package users::OA::Credential;

use base "Contenido::Document";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'Имя в системе' },
		{ 'attr' => 'status',		'type' => 'status',     'rusname' => 'Статус',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Подтвержден'],
					[3, 'Потерян / заблокирован'],
				],
		},
		{ 'attr' => 'ava_url',		'type' => 'url',	'rusname' => 'Аватар (url)' },
		{ 'attr' => 'avatar',		'type' => 'image',	'rusname' => 'Аватар' },
#		{ 'attr' => 'login',		'type' => 'string',	'rusname' => 'Login' },
#		{ 'attr' => 'passwd',		'type' => 'string',	'rusname' => 'Password' },
	)
}

sub class_name
{
	return 'OpenAuth Proto';
}

sub class_description
{
	return 'OpenAuth Proto class';
}

sub search_fields
{
	return ('ext_id', 'name', 'uid');
}

sub class_table
{
	return 'users::SQL::CredentialsTable';
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->main == 1 || $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:black;';
	}
}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{user_externals}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{user_externals};
	if ( $default_section && !$self->{sections} ) {
		$self->{sections} = $default_section;
	}

	return 1;
}
1;
