package users::OA::VK;

use base "users::OA::Credential";
use Contenido::Globals;

sub class_name
{
	return 'OpenAuth ВКонтакте';
}

sub class_description
{
	return 'OpenAuth ВКонтакте';
}

1;
