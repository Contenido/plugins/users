package users::OA::Mailru;

use base "users::OA::Credential";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'Имя в системе' },
		{ 'attr' => 'email',		'type' => 'string',	'rusname' => 'E-mail' },
		{ 'attr' => 'status',		'type' => 'status',     'rusname' => 'Статус',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Подтвержден'],
					[3, 'Потерян / заблокирован'],
				],
		},
	)
}

sub class_name
{
	return 'OpenAuth Mail.ru';
}

sub class_description
{
	return 'OpenAuth Mail.ru';
}

1;
