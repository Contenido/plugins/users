package users::OA::Google;

use base "users::OA::Credential";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'Имя в системе' },
		{ 'attr' => 'email',		'type' => 'string',	'rusname' => 'E-mail' },
		{ 'attr' => 'status',		'type' => 'status',     'rusname' => 'Статус',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Подтвержден'],
					[3, 'Потерян / заблокирован'],
				],
		},
	)
}

sub class_name
{
	return 'OpenAuth Google';
}

sub class_description
{
	return 'OpenAuth Google';
}

1;
