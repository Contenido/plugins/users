package users::SQL::UserProfile;

use base 'SQL::DocumentTable';
use Contenido::Globals;

sub db_table
{
        return 'profiles';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter

					_login_filter
					_email_filter
					_nickname_filter
			)];

sub available_filters {
	return $available_filters;
}

my @required_properties = (
		{						       # Идентификатор документа, сквозной по всем типам...
			'attr'		=> 'id',
			'type'		=> 'integer',
			'rusname'	=> 'Идентификатор документа',
			'hidden'	=> 1,
			'readonly'	=> 1,
			'auto'		=> 1,
			'db_field'	=> 'id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default nextval('public.documents_id_seq'::text)",
		},
		{						       # Класс документа...
			'attr'		=> 'class',
			'type'		=> 'string',
			'rusname'	=> 'Класс документа',
			'hidden'	=> 1,
			'readonly'	=> 1,
			'db_field'	=> 'class',
			'db_type'	=> 'varchar(48)',
			'db_opts'	=> 'not null',
		},
		{						       # Ф.И.О....
			'attr'		=> 'name',
			'type'		=> 'string',
			'rusname'	=> 'Ф.И.О.',
			'column'	=> 2,
			'db_field'	=> 'name',
			'db_type'	=> 'varchar(255)',
		},
		{						       # Время создания документа, служебное поле...
			'attr'		=> 'ctime',
			'type'		=> 'datetime',
			'rusname'       => 'Время создания',
			'readonly'      => 1,
			'auto'		=> 1,
			'hidden'	=> 1,
			'db_field'      => 'ctime',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{						       # Время модификации документа, служебное поле...
			'attr'		=> 'mtime',
			'type'		=> 'datetime',
			'rusname'       => 'Время модификации',
			'hidden'	=> 1,
			'auto'		=> 1,
			'db_field'      => 'mtime',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{						       # Дата рождения
			'attr'		=> 'dtime',
			'type'		=> 'date',
			'rusname'	=> 'Дата рождения',
			'db_field'      => 'dtime',
			'db_type'       => 'timestamp',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{						       # Дата и время логина...
			'attr'		=> 'lastlogin',
			'type'		=> 'datetime',
			'rusname'       => 'Дата и время последнего логина<sup style="color:#888;">&nbsp;1)</sup>',
			'column'	=> 1,
			'db_field'      => 'lastlogin',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{						       # Массив секций, обрабатывается специальным образом...
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'       => 'Секции',
			'hidden'	=> 1,
			'db_field'      => 'sections',
			'db_type'       => 'integer[]',
		},
		{						       # Одно поле статуса является встроенным...
			'attr'		=> 'status',
			'type'		=> 'status',
			'rusname'       => 'Статус',
			'db_field'      => 'status',
			'db_type'       => 'integer',
		},
		{							# Дополнительное поле статуса
			'attr'		=> 'type',
			'type'		=> 'integer',
			'rusname'	=> 'Тип пользователя (project-oriented)',
			'db_field'	=> 'type',
			'db_type'	=> 'integer',
		},
		{						       # Никнейм...
			'attr'		=> 'nickname',
			'type'		=> 'string',
			'rusname'	=> 'Ник',
			'column'	=> 3,
			'db_field'	=> 'nickname',
			'db_type'	=> 'text',
		},
		{						       # Login...
			'attr'		=> 'login',
			'type'		=> 'string',
			'rusname'	=> 'Логин',
			'column'	=> 4,
			'db_field'	=> 'login',
			'db_type'	=> 'text',
		},
		{                                                       # Login method...
			'attr'		=> 'login_method',
			'type'		=> 'string',
			'rusname'	=> 'Метод входа',
			'db_field'	=> 'login_method',
			'db_type'	=> 'text',
		},
		{						       # E-mail...
			'attr'		=> 'email',
			'type'		=> 'emails',
			'rusname'	=> 'E-mail (primary)',
			'hidden'	=> $state->{users}->use_credentials ? 1 : undef,
			'readonly'	=> $state->{users}->use_credentials ? 1 : undef,
			'db_field'	=> 'email',
			'db_type'	=> 'text',
		},

);

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	return @required_properties;
}

########### FILTERS DESCRIPTION ####################################################################################
sub _login_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists($opts{login}) );
	if (exists $opts{ilike} && $opts{ilike} == 1) {
		return &SQL::Common::_generic_name_filter('d.login', $opts{login}, 0, \%opts);
	}else{
		return &SQL::Common::_generic_text_filter('d.login', $opts{login});
	}
}

sub _email_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{email} );
	if ( $state->{users}->use_credentials ) {
		my ($wheres, $values);
		my $op = exists $opts{like} ? 'like' : exists $opts{ilike} ? 'ilike' : '=';
		$wheres = "d.id IN (SELECT uid FROM profile_credentials WHERE class = 'users::Email' AND name $op ?)";
		$values = [$opts{email}];
		return $wheres, $values;
	} else {
		if (exists $opts{ilike} && $opts{ilike} == 1) {
			return &SQL::Common::_generic_name_filter('d.email', $opts{email}, 0, \%opts);
		} else {
			return &SQL::Common::_generic_text_filter('d.email', $opts{email});
		}
	}
}

sub _nickname_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists($opts{nickname}) );
	if (exists $opts{ilike} && $opts{ilike} == 1) {
		return &SQL::Common::_generic_name_filter('d.nickname', $opts{nickname}, 0, \%opts);
	}else{
		return &SQL::Common::_generic_text_filter('d.nickname', $opts{nickname});
	}
}

1;
