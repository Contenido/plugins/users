package users::SQL::CredentialsTable;

use strict;
use base 'SQL::DocumentTable';
use Contenido::Globals;

sub db_table
{
	return 'profile_credentials';
}

sub available_filters {
	my @available_filters = qw(
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_sfilter_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter

					_excludes_filter
					_link_filter
					_uid_filter
					_uid_not_filter
					_ext_id_filter
					_main_filter
				);
	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'sections' && $_->{attr} ne 'dtime' } $self->SUPER::required_properties;
	return ( 
		@parent_properties,
		{
			'attr'		=> 'uid',
			'type'		=> 'pickup',
			'rusname'       => 'Идентификатор пользователя',
			'lookup_opts'	=> {
				'class'		=> $state->{users}->profile_document_class,
				'search_by'	=> 'name',
			},
			'db_field'      => 'uid',
			'db_type'       => 'integer',
			'db_opts'       => "not null default 0",
		},
		{
			'attr'		=> 'ext_id',
			'type'		=> 'integer',
			'rusname'       => 'External ID',
			'db_field'      => 'ext_id',
			'db_type'       => 'integer',
			'db_opts'       => "not null default 0",
			'default'	=> 0,
		},
		{
			'attr'		=> 'opaque',
			'type'		=> 'status',
			'rusname'       => 'Видимость',
			'cases'	=> [
				[0, 'не видно никому'],
				[1, 'видно близким людям'],
				[2, 'видно всем друзьям'],
				[3, 'видно всем пользователям'],
				[10, 'видно всем'],
			],
			'db_field'      => 'opaque',
			'db_type'       => 'integer',
		},
		{
			'attr'		=> 'confirm',
			'type'		=> 'string',
			'rusname'       => 'Confirmation string',
			'db_field'      => 'confirm',
			'db_type'       => 'varchar(32)',
		},
		{
			'attr'		=> 'main',
			'type'		=> 'checkbox',
			'rusname'       => 'Main credential',
			'db_field'      => 'main',
			'db_type'       => 'integer',
		},
		{
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'       => 'Секция',
			'hidden'	=> 1,
			'db_field'      => 'sections',
			'db_type'       => 'integer',
		},

	);
}


sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _uid_not_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid_not} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid_not}, 1);
}

sub _main_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{main} );
	return &SQL::Common::_generic_int_filter('d.main', $opts{main});
}

sub _ext_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{ext_id} );
	return &SQL::Common::_generic_int_filter('d.ext_id', $opts{ext_id});
}

sub _name_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{name} );
	if ( exists $opts{ext_id} ) {
		return &SQL::Common::_generic_text_filter('d.name', $opts{name});
	} else {
		my ($w1, $v1) = &SQL::Common::_generic_text_filter('d.name', $opts{name});
		my ($w2, $v2) = &SQL::Common::_generic_int_filter('d.ext_id', 0);
		return " ($w1) AND ($w2) ", [@$v1, @$v2];
	}
}

1;