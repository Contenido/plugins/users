package users::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use users::Apache;
use users::Keeper;


# загрузка всех необходимых плагину классов
# users::SQL::SomeTable
# users::SomeClass
Contenido::Init::load_classes(qw(
	users::SQL::UserProfile
	users::SQL::CredentialsTable

	users::UserProfile
	users::Email
	users::Phone
	users::Section

	users::OA::VK
	users::OA::FaceBook
	users::OA::Google
	users::OA::Mailru
	));

sub init {
	push @{ $state->{'available_documents'} }, 'users::UserProfile'		if $state->{users}->profile_document_class eq 'users::UserProfile';
	push @{ $state->{'available_sections'} }, 'users::Section';
	if ( $state->{users}->use_credentials ) {
		push @{ $state->{'available_documents'} }, 
				qw( users::Email users::Phone users::OA::VK users::OA::FaceBook users::OA::Google users::OA::Mailru );
	}
	0;
}

1;
