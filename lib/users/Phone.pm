package users::Phone;

use base "Contenido::Document";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'Телефон (только номер)' },
		{ 'attr' => 'status',		'type' => 'status',     'rusname' => 'Статус e-mail-адреса',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Подтвержден'],
					[3, 'Потерян / заблокирован'],
				],
		},
		{ 'attr' => 'name_format',	'type' => 'string',	'rusname' => 'Телефон отформатированный' },
		{ 'attr' => 'name_orig',	'type' => 'string',	'rusname' => 'Телефон (ввод пользователя)' },
#		{ 'attr' => 'login',		'type' => 'string',	'rusname' => 'Login' },
#		{ 'attr' => 'passwd',		'type' => 'string',	'rusname' => 'Password' },
	)
}

sub get_code {
    my $self = shift;
    if ( $self->name_format =~ /\((\d+)\)/ ) {
	return $1;
    }
}

sub get_number {
    my $self = shift;
    my $number = $self->name_format;
    for ( $number ) {
	s/^[\+\d+]\(/\(/;
	s/\(\d+\)//;
	s/\D//g;
    }
    return $number;
}

sub class_name
{
	return 'Телефон';
}

sub class_description
{
	return 'Телефон';
}

sub search_fields
{
	return ('name');
}

sub class_table
{
	return 'users::SQL::CredentialsTable';
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->main == 1 || $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:black;';
	}
}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{user_phones}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{user_phones};
	if ( $default_section && !$self->{sections} ) {
		$self->{sections} = $default_section;
	}

	return 1;
}
1;
