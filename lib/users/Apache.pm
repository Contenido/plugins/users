package users::Apache;

use strict;
use warnings 'all';

use users::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{users} = users::Keeper->new($state->users);
}

sub request_init {
}

sub child_exit {
}

1;
