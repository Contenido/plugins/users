create table profile_credentials
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 0,
	opaque smallint not null default 0,
	uid integer not null references profiles (id) on delete cascade,
	ext_id numeric not null default 0,
	sections integer,
	name text,
	main integer,
	confirm varchar(32),
	data text
);
create index profile_credentials_user_class_ext_id on profile_credentials (class, uid, ext_id) where ext_id != 0;
create index profile_credentials_user_class_name on profile_credentials (class, uid, name) where ext_id = 0;
create unique index profile_credentials_class_ext_id on profile_credentials (class, ext_id) where ext_id != 0;
create unique index profile_credentials_class_name on profile_credentials (class, name) where ext_id = 0;
