create table profiles
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp,
	lastlogin timestamp not null default now(),
	status smallint not null default 0,
	type integer,
	sections integer[],
	name text,
	nickname text,
	login text,
	email text,
	login_method text default 'local',
	data text
);
create index profiles_sections on profiles using gist ( "sections" "gist__int_ops" );
create index profiles_lastlogin on profiles (lastlogin);
create unique index profiles_login on profiles (login) where login is not null and login != '';
create unique index profiles_email on profiles (email) where email is not null and email != '';